namespace Blog2_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Posts", "Account_AccountID", "dbo.Accounts");
            DropIndex("dbo.Posts", new[] { "Account_AccountID" });
            RenameColumn(table: "dbo.Posts", name: "Account_AccountID", newName: "AccountID");
            AlterColumn("dbo.Accounts", "Email", c => c.String());
            AlterColumn("dbo.Accounts", "FirstName", c => c.String(maxLength: 100));
            AlterColumn("dbo.Accounts", "LastName", c => c.String(maxLength: 100));
            AddForeignKey("dbo.Posts", "AccountID", "dbo.Accounts", "AccountID", cascadeDelete: true);
            CreateIndex("dbo.Posts", "AccountID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Posts", new[] { "AccountID" });
            DropForeignKey("dbo.Posts", "AccountID", "dbo.Accounts");
            AlterColumn("dbo.Accounts", "LastName", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Accounts", "FirstName", c => c.String(nullable: false));
            AlterColumn("dbo.Accounts", "Email", c => c.String(nullable: false));
            RenameColumn(table: "dbo.Posts", name: "AccountID", newName: "Account_AccountID");
            CreateIndex("dbo.Posts", "Account_AccountID");
            AddForeignKey("dbo.Posts", "Account_AccountID", "dbo.Accounts", "AccountID");
        }
    }
}
