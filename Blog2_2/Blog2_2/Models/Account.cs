﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog2_2.Models
{
    public class Account
    {
        public int AccountID { set; get; }
        [Required]
        [DataType(DataType.Password)]
        public String Password { set; get; }
        
        [DataType(DataType.EmailAddress)]
        public String Email { set; get; }
        [StringLength(100, ErrorMessage = "Nhập tối đa 100 ký tự", MinimumLength = 1)]
        public String FirstName { set; get; }
        [StringLength(100, ErrorMessage = "Nhập tối đa 100 ký tự", MinimumLength = 1)]
        public String LastName { set; get; }
        
        public virtual ICollection<Post> Posts { set; get; }
    }
}