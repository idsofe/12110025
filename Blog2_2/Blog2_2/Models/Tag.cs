﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog2_2.Models
{
    public class Tag
    {
        public int TagID { set; get; }
        [Required]
        public String Content { set; get; }
        [Required]
        [StringLength(100, ErrorMessage = "Nhập vào từ 10 đến 100 ký tự", MinimumLength = 10)]
        public virtual ICollection<Post> Posts { set; get; }
    }
}