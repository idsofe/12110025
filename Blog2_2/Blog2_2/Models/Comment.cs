﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog2_2.Models
{
    public class Comment
    {
        public int ID { set; get; }
        [Required]
        public String Body { set; get; }
        [Required]
        [StringLength(5000, ErrorMessage = "Nhập tối đa 50 ký tự", MinimumLength = 50)]
        public DateTime DateCreated { set; get; }
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime DateUpdated { set; get; }
        [Required]
        [DataType(DataType.DateTime)]
        public String Author { set; get; }
        [Required]

        public int PostID { set; get; }
        public virtual Post Post { set; get; }
    }
}